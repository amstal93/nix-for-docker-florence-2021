{
    sources ? import ../nix/sources.nix,
    pkgs ? import sources.nixpkgs {}
}:
pkgs.dockerTools.buildImage {
  name = "hello_docker";
  tag = "minimal";
  created = "now";
  config.Cmd = [ "${pkgs.pkgsStatic.hello}/bin/hello" ];
}