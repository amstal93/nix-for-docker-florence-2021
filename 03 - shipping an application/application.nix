{ lib, fetchFromGitHub, buildGoModule, callPackage }:
buildGoModule  {
  pname = "gitops-greeter";
  version = "v0.0.1";

  src = fetchFromGitHub {
    owner = "locurateam";
    repo = "gitops-greeter";
    rev = "e2b7e4f19e01ea1ab5691426a34a071852776cb5";
    sha256 = "00xarf9x7rbz3jr2knwpdahy4d4wdvd88gjgvmdf89wmdmdxw84n";
  };

  vendorSha256 = "00ib7vfp44aw4njdmi3bkkr469naqdgbvbi63dxhjxjwmhjpwmkj";

  meta = with lib; {
    description = "Simple greeter with an early 2000 fashion";
    homepage = "https://github.com/locurateam/gitops-greeter";
    license = licenses.mit;
    maintainers = with maintainers; [ zarelit ];
    platforms = platforms.linux;
  };

}