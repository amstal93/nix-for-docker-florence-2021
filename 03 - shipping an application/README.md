# Full deployment example

## Package the application

We'll package gitops-greeter, which is a simple web app that greets you
and use redis as a dependency.

In application.nix is shown how is possible to package a
Go project that is a Go Module using a function called `buildGoModule` provided
to us by the nixpkgs project.
`buildGoModule` does a lot of things and basically can take applications in the
form of go modules (i.e. with  go.mod and go.sum files) , builds and packs
the dependencies and builds the executables.

gitops-greeter is simple enough to be able to be packaged via `buildGoModule`
without any additional step

Build with `nix build -f application.nix` and see that calling
`result/bin/greeter` prints an error for a missing environment variable.

## Make a launcher

The application by itself does nothing, because it depends on redis.

Let's write a wrapper that launches redis in the background and the
application in foreground.
Vendoring the external dependencies in a single docker image along with your application
is an anti-pattern but let's do this for the sake of experimentation pretending that
it's just intended to create an easy-to-user docker image
where you `docker run` and everything just works ™

See launcher.nix, notice that `app` is passed from `default.nix` and is built
with the previous expression.

## Put it in docker

With a launcher preparing the docker image is trivial because
it's just an image whose `CMD` is to launch the launcher and everything
works the same as hello_docker in the first example.

## Test the application without docker

Since we already packaged the application we can do different things rather than
just putting the application in docker.

For example we could use the NixOS test framework to test that our application
works with a specific version of redis.

In test.nix (`nix build -f test.nix`) we are going to spawn three qemu machines:
one with redis, one with the application server and one that will run a browser.

The script will wait for the services to come up, then fire a browser and
capture a screenshot of the running application.

Some lines will make test calls to the service in order to check if it answers 200
to the healthcheck endpoint.

If the application breaks the healthcheck API, `test.nix` will fail building
and we're reassured that everything is still working if it builds.

If there's an error in the tests, they can be debugged interactively
by building the interactive driver: `nix build -f test.nix driverInteractive`

