let
  sources = import ../nix/sources.nix;
  pkgs = import sources.nixpkgs {};
  test_framework = sources.nixpkgs + "/nixos/tests/make-test-python.nix";
  x11_profile = sources.nixpkgs + "/nixos/tests/common/x11.nix";
  app = pkgs.callPackage ./application.nix {};
in

import test_framework ({ pkgs, ... }:
{
  name = "gitops-greeter-test";

  nodes = {
    ## appnode will be the machine running the server
    appnode =
      { pkgs, ... }:
      {
        # Install app and start it on boot
        environment.systemPackages = [ app ];
        # Create a systemd unit to launch the app
        systemd.services.greeter = {
            wantedBy = ["multi-user.target"];
            script = "${app}/bin/greeter";
            environment = {
              "ENVIRONMENT" = "development";
              "REDIS_HOST" = "dbnode:6379";
              "SERVER_PORT"= "8080";
            };
        };
        networking.firewall.allowedTCPPorts = [ 8080 ] ;
      };

    ## dbnode will be the server running redis
    dbnode = { pkgs, config, ... }:
      {
        # Install redis
        services.redis.enable = true;
        services.redis.openFirewall = true;
        services.redis.bind = "0.0.0.0";
      };

    ## client will the machine that takes the screenshot
    client = { pkgs, ... }:
      {
            imports = [ x11_profile ];
            hardware.opengl.driSupport = true;
            environment.systemPackages = [ pkgs.midori ];
      };
  };

  testScript = ''
    # Start all the nodes 
    start_all()

    # Let's wait for redis
    dbnode.wait_for_unit("redis")
    dbnode.wait_for_open_port("6379")

    # Let's wait for the app to come up
    appnode.wait_for_unit("greeter")
    appnode.wait_for_open_port("8080")
    appnode.wait_until_succeeds("curl --fail http://localhost:8080/api/healtz")

    # Let's open the browser and take a screenshot!
    client.wait_for_x()
    client.execute("midori -e Fullscreen http://appnode:8080 &")

    dbnode.sleep(60) # Wait a bit....

    client.screenshot("app_screenshot_from_test")
  '';
})